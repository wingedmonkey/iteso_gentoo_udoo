#!/bin/bash
# Iteso

2>&1 >> gentoo_udoo.log

#VERIFY ARGUMENTS PASSED
if [ "$#" -ne 1 ]; then 
    echo "GENTOO_UDOO[ERROR]: Udoo board version missing."
    exit -1
else
    UDOO_V=$1
    echo "GENTOO_UDOO: Udoo board version is UDOO_$UDOO_V"
fi

#SWITCH TO BOARD BRANCH
if [ $UDOO_V == "NEO_FULL" ]; then
    UBOOT_BRANCH=2015.04.imx-neo
    UDOO_MAKE_CFG=udoo_neo_config
    UDOO_KDEFCFG=udoo_neo_defconfig
elif [ $UDOO_V == "QUAD" ]; then
    UBOOT_BRANCH=2015.10.fslc-qdl
    UDOO_MAKE_CFG=udoo_qdl_config
    UDOO_KDEFCFG=udoo_quad_dual_defconfig
else
    echo "GENTOO_UDOO[ERROR]: Unknown udoo board version."
    exit -1
fi


#DEFINE GLOBAL VARIABLES
BLD_CMD="ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf-"
UBOOT_V="uboot2015"
KERNEL_V="3.15.1"
KERNEL_LINK="https://www.kernel.org/pub/linux/kernel/v3.x/linux-${KERNEL_V}.tar.gz"
KERNEL_SRC_FILE="linux-${KERNEL_V}.tar.gz"
KERNEL_SRC_DIR="linux-${KERNEL_V}"
KERNEL_BLD_CMD="make ${BLD_CMD}"
UDOO_CFG_CMD="${BLD_CMD} make ${UDOO_MAKE_CFG}"
UDOO_BLD_CMD="${BLD_CMD} make"
ROOTFS_SRC_DIR="stage4-armv7a_hardfp-rootfs-20140614"
ROOTFS_SRC_FILE="${ROOTFS_SRC_DIR}.tar.bz2"
ROOTFS_LINK="http://www.gentoogeek.org/files/armv7-multiplatform/${ROOTFS_SRC_FILE}"


#RUN apt-get UPDATES
echo "GENTOO_UDOO: Need sudo to run apt-get update, please provide pwd."
sudo apt-get update

#INSTALL GIT IF NOT AVAILABLE
if [ -z $(which git | grep git) ]; then
    echo "GENTOO_UDOO: git not found. Attempting to install."
    sudo apt-get install git
else
    echo "GENTOO_UDOO: git available"
fi

#CREATE WORK DIRECTORY
if [ -d "gentoo_udoo" ]; then
    cd gentoo_udoo
else
    mkdir gentoo_udoo
    cd gentoo_udoo
fi

#SAVE WORK_DIR
WORK_DIR=`pwd`
echo "work dir: ${WORK_DIR}"

#DOWNLOAD UBOOT SOURCES
echo "GENTOO_UDOO: Getting u-boot"
if [ -d "uboot" ]; then
    cd uboot 
else
    mkdir uboot 
    cd uboot
fi
if [ ! -d $UBOOT_V ]; then
    git clone https://github.com/UDOOboard/uboot-imx.git $UBOOT_V    
fi
if [ ! -d $UBOOT_V ]; then
    echo "GENTOO_UDOO[ERROR]: Could not clone UBOOT source ${UBOOT_V}."
    exit -1
fi

#SWITCH TO BOARD BRANCH
cd "${UBOOT_V}"
git checkout $UBOOT_BRANCH    

echo "GENTOO_UDOO: Running u-boot configuration"
echo "GENTOO_UDOO: ${UDOO_CFG_CMD}"
eval ${UDOO_CFG_CMD}
echo "GENTOO_UDOO: Running u-boot build"
echo "GENTOO_UDOO: ${UDOO_BLD_CMD}"
eval ${UDOO_BLD_CMD}


#GET KERNEL
echo "GENTOO_UDOO: Getting kernel ${KERNEL_V} sources"
if [ ! -d "$WORK_DIR/kernel" ]; then
    mkdir $WORK_DIR/kernel
fi
if [ ! -d "$WORK_DIR/kernel" ]; then
    echo "GENTOO_UDOO[ERROR]: Could not create work directory for kernel sources"
    exit -1
else
    cd "${WORK_DIR}/kernel"
fi

if [ ! -d "${KERNEL_SRC_DIR}" ]; then
    if [ ! -f "${KERNEL_SRC_FILE}" ]; then
        wget $KERNEL_LINK
    fi
    if [ ! -f "${KERNEL_SRC_FILE}" ]; then
        echo "GENTOO_UDOO[ERROR]: Could not download kernel source file ${KERNEL_SRC_FILE}"
        exit -1
    fi
    tar -zxvf $KERNEL_SRC_FILE
fi
if [ -d "${KERNEL_SRC_DIR}" ]; then
    cd "${KERNEL_SRC_DIR}"
else
    echo "GENTOO_UDOO[ERROR]: Could not extract kernel sources"
    exit -1
fi



cp "${WORK_DIR}/../udoo_neo_defconfig" "arch/arm/configs/"
cp "${WORK_DIR}/../udoo_quad_dual_defconfig" "arch/arm/configs/"

make ${BLD_CMD} ${UDOO_KDEFCFG}
#make ${BLD_CMD} menuconfig
make ${BLD_CMD} -j4 zImage
make ${BLD_CMD} dtbs
make ${BLD_CMD} INSTALL_MOD_PATH=output modules
make ${BLD_CMD} INSTALL_MOD_PATH=output modules_install
make ${BLD_CMD} firmware_install INSTALL_FW_PATH=output/firmware
make ${BLD_CMD} headers_install INSTALL_HDR_PATH=output/headers


#GET FILE SYSTEM 
echo "GENTOO_UDOO: Getting gentoo file system"
if [ ! -d "$WORK_DIR/rootfs" ]; then
    mkdir $WORK_DIR/rootfs
fi
if [ ! -d "$WORK_DIR/rootfs" ]; then
    echo "GENTOO_UDOO[ERROR]: Could not create work directory for kernel sources"
    exit -1
else
    cd "${WORK_DIR}/rootfs"
fi

if [ ! -d "${ROOTFS_SRC_DIR}" ]; then
    if [ ! -f "${ROOTFS_SRC_FILE}" ]; then
        wget $ROOTFS_LINK
    fi
    if [ ! -f "${ROOTFS_SRC_FILE}" ]; then
        echo "GENTOO_UDOO[ERROR]: Could not download kernel source file ${ROOTFS_SRC_FILE}"
        exit -1
    fi
    tar -xvjf $ROOTFS_SRC_FILE
fi
if [ -d "${ROOTFS_SRC_DIR}" ]; then
    cd "${ROOTFS_SRC_DIR}"
else
    echo "GENTOO_UDOO[ERROR]: Could not extract kernel sources"
    exit -1
fi

#flash uboot
cd "${WORK_DIR}/uboot/${UBOOT_V}"
export DISK=/dev/sdb
sudo dd if=SPL of=${DISK} bs=1k seek=1
sudo dd if=u-boot.img of=${DISK} bs=1K seek=69
sudo sync

#CREATE PARTITIONS
cd "${WORK_DIR}"
sudo fdisk ${DISK} < fdisk_opt
sudo mkfs.vfat -n KERNEL ${DISK}1
sudo mkfs.ext3 -n ROOTFS ${DISK}2

#mount partitions
sudo mkdir /media/udoo_boot
sudo mkdir /media/udoo_rootfs
sudo mount ${DISK}1 /media/udoo_boot
sudo mount ${DISK}2 /media/udoo_rootfs
if [ ! -d "/media/udoo_rootfs/lib/modules" ]; then
    mkdir /media/udoo_rootfs/lib/modules
fi
if [ ! -d "/media/udoo_rootfs/lib/firmware" ]; then
    mkdir /media/udoo_rootfs/lib/firmware
fi

#copy kernel files
sudo cp "${WORK_DIR}/kernel${KERNEL_SRC_DIR}/arch/arm/boot/dts/*udoo*" /media/udoo_boot/dts
sudo cp "${WORK_DIR}/kernel${KERNEL_SRC_DIR}/arch/arm/boot/zImage" /media/udoo_boot

#copy file system files
sudo cp -r "${WORK_DIR}/rootfs/${ROOTFS_SRC_DIR}/*" /media/udoo_rootfs
sudo cp "${WORK_DIR}/kernel/${KERNEL_SRC_DIR}/output/lib/modules/${KERNEL_V}" /media/udoo_rootfs/lib/modules

#copy headers
sudo cp "${WORK_DIR}/kernel/${KERNEL_SRC_DIR}/output/headers/include"  /media/udoo_rootfs/usr/
sudo cp "${WORK_DIR}/kernel/${KERNEL_SRC_DIR}/output/firmware/*"  /media/udoo_rootfs/lib/firmware

sudo sync
